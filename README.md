# afrl-pipelines

## Purpose

The pipelines in this repository are designed to interface with the AFRL-provided applications in your DevSecOps environment.

Each pipeline listed below uses a [semantic versioning schema](semver.org).
Major versions may have new features added over time, but breaking changes will not be added to existing released versions.

To use any of the pipelines in this repository, use the GitlabCI `include` keyword in your pipeline's file,
then reference the "raw" URL of the pipeline file to import and include it into yours.

```
include: https://${this_repo}/-/raw/main/${pipeline_name}/${pipeline_version}/gitlab-ci.yml

```

You will also need to add the stages listed at the top of each pipeline file you include into your stages at the appropriate spot in the list.
Remember, Gitlab will execute jobs in the order the stages are listed under the `stages:` keyword (by default).

```
stages:
  - my-build-stage
  - afrl-imported-stage-1
  - my-test-stage
  - afrl-imported-stage-2

```

See the `README.md` in each pipeline version's directory for details about required variables for that pipeline.

```
variables:
  AFRL_SAMPLE_VARIABLE: "sample value for this variable"
```

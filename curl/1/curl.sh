#!/bin/bash
#

afrlCurlMain() {

  local url
  local response_code_only
  local curl_successful
  local curl_output_file
  local artifacts_directory
  url="${AFRL_CURL_URL}"
  response_code_only="${AFRL_CURL_RESPONSE_CODE_ONLY:=false}"
  curl_output_file="curl-output-file.txt"
  artifacts_directory="./artifacts"

  if $response_code_only; then
    echo -e "\n[INFO] ${FUNCNAME[0]} | Response code only requested."
    # Add '-I' to curl args
    curl_args+=("-I") || return 1
  fi

  echo -e "\n[INFO] ${FUNCNAME[0]} | Designating curl output file location ($curl_output_file)."
  # Add '-O $curl_output_file' to curl args
  curl_args+=("-o" "$curl_output_file") || return 1

  if [[ "${url}" == "" ]]; then
    echo -e "\n[FAIL] ${FUNCNAME[0]} | User failed to pass a URL string." 1>&2
    return 2
  else
    # Add url to curl args
    echo -e "\n[INFO] ${FUNCNAME[0]} | User passed a URL string ($url)."
    curl_args+=("${url}") || return 1
  fi

  echo -e "\n[INFO] ${FUNCNAME[0]} | Running curl with args (curl ${curl_args[*]})."
  { curl "${curl_args[@]}" && curl_successful=true; } || curl_successful=false

  if $curl_successful; then
    echo -e "\n[INFO] ${FUNCNAME[0]} | Curl command was successful! :-)"
  else
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Curl command was not successful! :-(" 1>&2
    return 3
  fi

  echo -e "\n[INFO] ${FUNCNAME[0]} | Saving curl output file ($curl_output_file) as artifact in artifacts directory ($artifacts_directory)."
  mkdir -p "$artifacts_directory" || return 1
  mv "$curl_output_file" "$artifacts_directory" || return 1

  echo -e "\n[INFO] ${FUNCNAME[0]} | Displaying curl output file ($curl_output_file) content."
  echo "$(<$curl_output_file)" || return 1

}

afrlCurlMain || exit "$?"

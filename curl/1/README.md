# Curl Pipeline

## Purpose

This is a pipeline that runs a `curl` against a provided URL.

## Usage

Add the following lines into your main pipeline.

```
...
include: https://gitlab.com/codrcodz/afrl-pipelines/-/raw/main/curl/1/gitlab-ci.yml

variables:
  AFRL_CURL_URL: "example.com"

stages:
  - afrl-curl
...
```

Ensure the "afrl-curl" stage is placed in the appropriate location relative to other stages.
See "Inputs" section below to see all required/optional input variables and/or files. 

## Inputs

### Variables

#### `AFRL_CURL_URL` Variable

The `AFRL_CURL_URL` variable is URL passed to `curl` that it returns its response/content for.

This variable is *required*. The format is a *string*. There is no default value.

#### `AFRL_CURL_RESPONSE_CODE_ONLY`

The `AFRL_CURL_RESPONSE_CODE_ONLY` variable tells `curl` whether or not to return only the HTTP response code instead of content.

The variable is optional. The format is *boolean*. The default value is "false".
